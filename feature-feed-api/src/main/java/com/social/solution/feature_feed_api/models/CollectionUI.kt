package com.social.solution.feature_feed_api.models

data class CollectionUI(
    var drawableResource: Int,
    var name: String,
    var description: String?,
    var isSelected: Boolean
)