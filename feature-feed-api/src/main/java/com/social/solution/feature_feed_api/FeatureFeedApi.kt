package com.social.solution.feature_feed_api

import androidx.lifecycle.MutableLiveData

interface FeatureFeedApi {
    fun feedStarter(): FeatureFeedStarter
    fun isAuthorized(): MutableLiveData<Boolean>
}