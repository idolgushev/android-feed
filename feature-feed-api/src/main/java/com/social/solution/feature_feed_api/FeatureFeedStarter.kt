package com.social.solution.feature_feed_api

import androidx.fragment.app.FragmentManager
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.core_backend_api.feedMemus.FeedMemusApi

interface FeatureFeedStarter {

    fun start(
        manager: FragmentManager, container: Int, addToBackStack: Boolean,
        callback: FeatureFeedCallback, feedMemusApi: FeedMemusApi)
}
