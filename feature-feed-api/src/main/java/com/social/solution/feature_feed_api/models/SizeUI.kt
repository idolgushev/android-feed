package com.social.solution.feature_feed_api.models

data class SizeUI (var width: Int, var height: Int)