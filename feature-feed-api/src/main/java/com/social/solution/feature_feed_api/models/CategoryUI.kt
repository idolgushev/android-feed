package com.social.solution.feature_feed_api.models

data class CategoryUI(val name: String, var isChecked: Boolean)