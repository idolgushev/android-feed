package com.social.solution.feature_feed_api

import com.social.solution.feature_feed_api.models.PostUI

interface FeatureFeedCallback {

    fun share(post: PostUI)
}