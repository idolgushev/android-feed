package com.social.solution.feature_feed_api.models

data class OwnerUI(val name: String, val avatar: String, var isSubscribe: Boolean)