package com.social.solution.feature_feed_api.models

import com.morozov.core_backend_api.feedMemus.model.MemTypeModel

data class PostUI(
    val id: String,
    val type: MemTypeModel,
    val name: String?,
    val date: String,
    val avatar: String,
    val post: List<String>,
    var isLiked: Boolean,
    val isBookmark: Boolean,
    var countLike: Int?,
    val countComment: Int?
)