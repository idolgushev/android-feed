package com.social.solution.androidfeed

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.models.PostUI
import com.social.solution.feature_feed_impl.LibFeedDependency

class MainActivity : AppCompatActivity() {

    private lateinit var featureBackendApi: FeatureBackendApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        featureBackendApi = LibBackendDependency.featureBackendApi(applicationContext)
        startFeedFragment()
    }


    private fun startFeedFragment() {
        val featureFeedApi =
            LibFeedDependency.featureFeedApi(applicationContext, featureBackendApi)
        featureFeedApi.feedStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            FeedCallback(),
            featureBackendApi.feedMemusApi()
        )
    }

    inner class FeedCallback : FeatureFeedCallback {

        override fun share(post: PostUI) {
            Toast.makeText(baseContext,"OPEN_SHARE_MODULE" ,Toast.LENGTH_SHORT).show()
        }
    }
}
