package com.social.solution.feature_feed_impl.utils

import com.morozov.core_backend_api.feedMemus.model.FeedCategoryModel
import com.morozov.core_backend_api.feedMemus.model.PostModel
import com.social.solution.feature_feed_api.models.CategoryUI
import com.social.solution.feature_feed_api.models.PostUI

fun convertFrom(category: FeedCategoryModel): CategoryUI {
    return CategoryUI(category.name, false)
}

fun convertFrom(post: PostModel): PostUI {
    return PostUI(
        post.id,
        post.memType,
        post.owner.userName,
        post.date,
        post.owner.avatar,
        post.urls,
        post.isLike,
        post.isBookmark,
        post.likeCount,
        post.commentCount
    )
}
