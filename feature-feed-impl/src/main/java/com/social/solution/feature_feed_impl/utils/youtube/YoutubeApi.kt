package com.social.solution.feature_feed_impl.utils.youtube

interface YoutubeApi {
    fun showVideo(videoId: String, startSecond: Float = 0f)
    fun start()
}