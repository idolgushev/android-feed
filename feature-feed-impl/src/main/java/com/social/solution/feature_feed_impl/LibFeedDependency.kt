package com.social.solution.feature_feed_impl

import android.content.Context
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedApi
import com.social.solution.feature_feed_impl.start.FeedStarterImpl

object LibFeedDependency {

    fun featureFeedApi(context: Context, backend: FeatureBackendApi): FeatureFeedApi {
        return FeedFeatureImpl(FeedStarterImpl(), context, backend)
    }
}