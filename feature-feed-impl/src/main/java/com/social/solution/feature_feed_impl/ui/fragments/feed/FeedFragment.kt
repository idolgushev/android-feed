package com.social.solution.feature_feed_impl.ui.fragments.feed

import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_api.models.CategoryUI
import com.social.solution.feature_feed_api.models.CollectionUI
import com.social.solution.feature_feed_api.models.PostUI
import com.social.solution.feature_feed_impl.ui.MainObject
import com.social.solution.feature_feed_impl.ui.adapters.FeedCategoryAdapter
import com.social.solution.feature_feed_impl.ui.adapters.FeedCollectionAdapter
import com.social.solution.feature_feed_impl.ui.adapters.FeedPostAdapter
import kotlinx.android.synthetic.main.feed_fragment.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class FeedFragment : Fragment() {


    private lateinit var callback: (PostUI) -> Unit
    private var position = 0
    private lateinit var feedAdapter: FeedPostAdapter
    private lateinit var feedViewModel: FeedViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        feedViewModel = ViewModelProviders.of(this).get(FeedViewModel::class.java)
        return inflater.inflate(R.layout.feed_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initCollection()
        this.initCategorise()
    }

    private fun initCategorise() {
        GlobalScope.launch(Dispatchers.Main) {
            var categoryList = listOf<CategoryUI>()
            val categoryAdapter = withContext(Dispatchers.IO) {
                FeedCategoryAdapter(categoryList)
            }
            val categoryBetweenItemsDecorator = withContext(Dispatchers.IO) {
                DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL).apply {
                    setDrawable(
                        ContextCompat.getDrawable(
                            context!!,
                            R.drawable.feed_category_recycler_view_decorator
                        )!!
                    )
                }
            }
            feedViewModel.loadCategories(MainObject.mfeedApi)
            feedViewModel.getCategories()
                .observe(this@FeedFragment, Observer<List<CategoryUI>> { categories ->
                    Log.i("FEED_VIEW_MODEL", "OBSERVE + $categories")
                    categories.forEach { it.isChecked = false }
                    categories[0].isChecked = true
                    categoryList = categories
                    categoryAdapter.categories = categoryList
                    categoryAdapter.notifyDataSetChanged()
                })
            this@FeedFragment.initFeed(categoryAdapter, categoryBetweenItemsDecorator)
        }
    }

    private fun initCollection() {
        val collections = listOf(
            CollectionUI(
                R.drawable.feed_collection_icon_best_memes,
                "Best memes",
                "Update 1.01.10",
                true
            ),
            CollectionUI(
                R.drawable.feed_collection_icon_collective,
                "Collective",
                "All memes feed",
                false
            ),
            CollectionUI(R.drawable.feed_collection_icon_favourite, "Favourite", null, false)
        )
        val adapterCollection =
            FeedCollectionAdapter(
                context!!,
                collections
            ) { collection ->
                feed_toolbar.title = collection.name
                feed_recycler_view_collection.visibility = View.GONE
            }
        feed_recycler_view_collection.adapter = adapterCollection

        feed_toolbar.setOnClickListener {
            if (feed_recycler_view_collection.visibility == View.VISIBLE) {
                feed_recycler_view_collection.visibility = View.GONE
            } else {
                adapterCollection.notifyDataSetChanged()
                feed_recycler_view_collection.visibility = View.VISIBLE
            }
        }
        Log.i("TEST_CLICK", "aaa")
        feed_recycler_view_collection.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                feed_recycler_view_collection.visibility = View.GONE
            }
            return@setOnTouchListener true
        }
    }

    private fun initFeed(
        categoryAdapter: FeedCategoryAdapter,
        categoryBetweenItemsDecorator: DividerItemDecoration
    ) {
        var feedList = mutableListOf<PostUI>()
        GlobalScope.launch(Dispatchers.Main) {
            withContext(Dispatchers.IO) {
                feedAdapter = FeedPostAdapter(
                    context!!, feedList, lifecycle, object :
                        FeedPostAdapter.FeedAdapterCallback {
                        override fun likePost(
                            post: PostUI,
                            isLiked: Boolean,
                            position: Int,
                            callback: (PostUI) -> Unit
                        ) {
                            this@FeedFragment.position = position
                            this@FeedFragment.callback = callback
                            feedViewModel.like(MainObject.mfeedApi, post, !post.isLiked)
                        }

                        override fun openShareView(post: PostUI) {
                            this@FeedFragment.sharePost(post)
                        }

                        override fun initCategories() {

                        }

                    }, categoryBetweenItemsDecorator, categoryAdapter
                )
            }
            val postBetweenItemsDecorator = withContext(Dispatchers.IO) {
                object : DividerItemDecoration(context, VERTICAL) {

                    override fun getItemOffsets(
                        outRect: Rect,
                        view: View,
                        parent: RecyclerView,
                        state: RecyclerView.State
                    ) {
                        val position: Int = parent.getChildAdapterPosition(view)
                        if (position == 0) {
                            outRect.setEmpty()
                        } else {
                            if (position == state.itemCount - 1)

                                outRect.set(0, 0, 0, 150)
                            else
                                super.getItemOffsets(outRect, view, parent, state)
                        }
                    }
                }.apply {
                    setDrawable(
                        ContextCompat.getDrawable(
                            context!!,
                            R.drawable.feed_post_recycler_view_decorator
                        )!!
                    )
                }
            }
            feed_recycler_view_post?.addItemDecoration(postBetweenItemsDecorator)

            feedViewModel.getUpdatePost()
                .observe(this@FeedFragment, Observer<PostUI> { postReceive ->
                    //                    Log.i("FEED_LIST", "pos = $position")
//                    feedList.forEach { Log.i("FEED_LIST", "name = ${it.name} postId = ${it.id}") }
                    callback(postReceive)
                })
            feedViewModel.getPosts().observe(this@FeedFragment, Observer<List<PostUI>> { posts ->
                feedList = posts.toMutableList()
                feedAdapter.posts = feedList
                Log.i("FEED_VIEW_MODEL", "OBSERVE + $posts")
                feedAdapter.notifyDataSetChanged()
            })
            feedViewModel.loadPosts(MainObject.mfeedApi)

            feed_recycler_view_post?.adapter = feedAdapter
        }

    }

    fun sharePost(post: PostUI) {
        MainObject.mCallback?.share(post)
    }
}

