package com.social.solution.feature_feed_impl.start

import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeatureFeedStarter
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.MainObject

class FeedStarterImpl : FeatureFeedStarter {

    override fun start(
        manager: FragmentManager, container: Int, addToBackStack: Boolean,
        callback: FeatureFeedCallback, feedMemusApi: FeedMemusApi
    ) {
        MainObject.mCallback = callback
        MainObject.mfeedApi = feedMemusApi
        val finalHost = NavHostFragment.create(R.navigation.nav_host_feed)
        manager.beginTransaction()
            .replace(container, finalHost)
            .setPrimaryNavigationFragment(finalHost)
            .commitAllowingStateLoss()
    }
}