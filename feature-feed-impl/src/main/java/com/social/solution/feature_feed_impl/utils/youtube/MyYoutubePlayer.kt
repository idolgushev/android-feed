package com.social.solution.feature_feed_impl.utils.youtube

import androidx.lifecycle.Lifecycle
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView

class MyYoutubePlayer(private val mYouTubePlayerView: YouTubePlayerView): YoutubeApi {

    companion object {
        private const val TAG = "YoutubeFragment"

        fun create(youTubePlayerView: YouTubePlayerView,
                   lifecycle: Lifecycle,
                   callback: YoutubeCallback? = null): YoutubeApi {
            val player = MyYoutubePlayer(youTubePlayerView)
            player.mCallback = callback
            lifecycle.addObserver(youTubePlayerView)
            return player
        }

        fun create(youTubePlayerView: YouTubePlayerView,
                   lifecycle: Lifecycle,
                   videoId: String,
                   callback: YoutubeCallback? = null): YoutubeApi {
            val player = MyYoutubePlayer(youTubePlayerView)
            player.mCallback = callback
            player.mVideoId = videoId
            lifecycle.addObserver(youTubePlayerView)
            return player
        }
    }

    private var mVideoId: String? = null
    private var mCallback: YoutubeCallback? = null
    private var mYouTubePlayer: YouTubePlayer? = null

    override fun start() {
        mYouTubePlayerView.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
            override fun onReady(youTubePlayer: YouTubePlayer) {
                mYouTubePlayer = youTubePlayer
                mVideoId?.let { showVideo(it, 0f) }
                mCallback?.onReady()
            }
        })
    }

    // YoutubeApi impl
    override fun showVideo(videoId: String, startSecond: Float) {
        mVideoId = videoId
        val youTubePlayer = mYouTubePlayer ?: return
        youTubePlayer.loadVideo(videoId, startSecond)
    }
}