package com.social.solution.feature_feed_impl.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_api.models.CategoryUI

class FeedCategoryAdapter(var categories: List<CategoryUI>) :
    RecyclerView.Adapter<FeedCategoryAdapter.CategoryViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {

        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.feed_card_category, parent, false)
        return CategoryViewHolder(
            view
        )
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.onBind(position, categories, this)
    }


    class CategoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val checkBox = view.findViewById<CheckBox>(R.id.feed_category_checkbox)

        fun onBind(
            position: Int,
            categoryList: List<CategoryUI>,
            feedCategoryAdapter: FeedCategoryAdapter
        ) {
            val category = categoryList[position]
            checkBox.text = category.name
            checkBox.setOnCheckedChangeListener { _, _ -> }
            checkBox.isChecked = category.isChecked

            checkBox.setOnCheckedChangeListener { _, isChecked ->
                if (!category.isChecked) {
                    category.isChecked = true
                    for (i in categoryList.indices) {
                        if (i != position)
                            categoryList[i].isChecked = false
                    }
                }

                feedCategoryAdapter.notifyDataSetChanged()

//                if (categoryList.indexOf(category) == 0) {
//                    if (isChecked) {
//                        for (i in categoryList.indices) {
//                            categoryList[i].isChecked = category.name == categoryList[i].name
//                        }
//                    } else {
//                        category.isChecked = true
//                    }
//                    feedCategoryAdapter.notifyDataSetChanged()
//                } else {
//                    categoryList[0].isChecked = false
//                    if (categoryList.filter { it.isChecked }.size != 1) {
//                        category.isChecked = isChecked
//                    } else {
//                        category.isChecked = true
//                    }
//                    feedCategoryAdapter.notifyDataSetChanged()
//                }
            }
        }
    }

}
