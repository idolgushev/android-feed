package com.social.solution.feature_feed_impl.ui.adapters

import android.content.Context
import android.view.LayoutInflater.from
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.morozov.core_backend_api.feedMemus.model.MemTypeModel
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_api.models.PostUI
import com.social.solution.feature_feed_impl.utils.youtube.MyYoutubePlayer
import kotlinx.android.synthetic.main.feed_card_categories.view.*
import kotlinx.android.synthetic.main.feed_card_post.view.*
import java.text.SimpleDateFormat

const val CATEGORIES_TYPE = 0
const val POST_TYPE = 1

class FeedPostAdapter(
    private val context: Context,
    var posts: List<PostUI>,
    private val mLifecycle: Lifecycle,
    private val feedAdapterCallback: FeedAdapterCallback,
    private val categoryBetweenItemsDecorator: RecyclerView.ItemDecoration,
    private val categoryAdapter: FeedCategoryAdapter
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == CATEGORIES_TYPE) {
            CategoriesViewHolder(
                from(parent.context).inflate(R.layout.feed_card_categories, parent, false)
            )
        } else {
            PostViewHolder(from(parent.context).inflate(R.layout.feed_card_post, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            CATEGORIES_TYPE
        } else {
            POST_TYPE
        }
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 0)
            (holder as CategoriesViewHolder).onBind(
                context,
                categoryBetweenItemsDecorator,
                categoryAdapter,
                mLifecycle,
                feedAdapterCallback
            )
        else
            (holder as PostViewHolder).onBind(
                context,
                position,
                posts,
                mLifecycle,
                feedAdapterCallback
            )
    }

    class CategoriesViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun onBind(
            context: Context,
            categoryBetweenItemsDecorator: RecyclerView.ItemDecoration,
            categoryAdapter: FeedCategoryAdapter,
            lifecycle: Lifecycle,
            feedAdapterCallback: FeedAdapterCallback
        ) {
            itemView.feed_category_list?.addItemDecoration(categoryBetweenItemsDecorator)
            itemView.feed_category_list?.adapter = categoryAdapter
        }
    }

    class PostViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(
            context: Context,
            position: Int,
            posts: List<PostUI>,
            lifecycle: Lifecycle,
            feedAdapterCallback: FeedAdapterCallback
        ) {
            val post = posts[position]
            view.name_user.text = post.name
            val postContent = post.post.firstOrNull()
            if (postContent != null)
                when (post.type) {
                    MemTypeModel.image -> {
                        itemView.youtube_player_view.visibility = View.GONE
                        itemView.post_image.visibility = View.VISIBLE
                        Glide
                            .with(itemView.post_image.context)
                            .asBitmap()
                            .transition(BitmapTransitionOptions.withCrossFade(1000))
                            .placeholder(R.drawable.place_holder)
                            .load(post.post[0])
                            .thumbnail(0.1f)
                            .skipMemoryCache(true)
                            .into(itemView.post_image)
                    }

                    MemTypeModel.video -> {
                        itemView.youtube_player_view.visibility = View.VISIBLE
                        itemView.post_image.visibility = View.GONE
                        MyYoutubePlayer.create(
                            itemView.youtube_player_view,
                            lifecycle,
                            post.post[0]
                        ).start()
                    }
                    MemTypeModel.gif -> {
                        itemView.youtube_player_view.visibility = View.GONE
                        itemView.post_image.visibility = View.VISIBLE
                        Glide
                            .with(itemView.post_image.context)
                            .asGif()
                            .transition(withCrossFade(1000))
                            .placeholder(R.drawable.place_holder)
                            .load(post.post[0])
                            .thumbnail(0.1f)
                            .skipMemoryCache(true)
                            .into(itemView.post_image)
                    }
                    else -> {
                        itemView.youtube_player_view.visibility = View.GONE
                        itemView.post_image.visibility = View.INVISIBLE
                    }
                }
            Glide
                .with(view.icon_image.context)
                .asDrawable()
                .placeholder(R.drawable.place_holder)
                .load(post.avatar)
                .into(view.icon_image)

            view.date_post.text = getStringTimeAgo(post.date)
            view.count_like.text = post.countLike.toString()
            view.count_comment.text = post.countComment.toString()
            chooseLike(context, post)
            view.like_frame.setOnClickListener {
                feedAdapterCallback.likePost(post, post.isLiked, position) { newPostUIData ->
                    view.count_like.text = newPostUIData.countLike.toString()
                    posts[position].isLiked = newPostUIData.isLiked
                    posts[position].countLike = newPostUIData.countLike
                    chooseLike(context, newPostUIData)
                }
            }
            view.comment_frame.setOnClickListener {

            }
            view.share_frame.setOnClickListener {
                feedAdapterCallback.openShareView(post)
            }

        }

        private fun getStringTimeAgo(date: String): String {
            val sdf = SimpleDateFormat("dd.MM.yyyy HH:mm:ss")
            val time = sdf.parse(date)?.time
            val currentTime = System.currentTimeMillis()
            val timeAgo = currentTime - time!!
            val secondAgo = timeAgo / 1000
            val minutesAgo = timeAgo / (1000 * 60)
            val hoursAgo = timeAgo / (1000 * 60 * 60)
            val daysAgo = timeAgo / (1000 * 60 * 60 * 24)
            var dateResult = date
            if (daysAgo >= 1) {
                dateResult = if (daysAgo == 1L)
                    "$daysAgo day ago"
                else "$daysAgo days ago"
            } else {
                if (hoursAgo >= 1) {
                    dateResult = if (hoursAgo == 1L)
                        "$hoursAgo hour ago"
                    else "$hoursAgo hours ago"
                } else {
                    if (minutesAgo >= 1) {
                        dateResult = if (minutesAgo == 1L)
                            "$minutesAgo minute ago"
                        else "$minutesAgo minutes ago"
                    } else {
                        if (secondAgo >= 1) {
                            dateResult = if (secondAgo == 1L)
                                "$secondAgo second ago"
                            else "$secondAgo seconds ago"
                        }
                    }
                }
            }
            return dateResult
        }

        private fun chooseLike(context: Context, post: PostUI) {
            if (post.isLiked) {
                view.like_image.setColorFilter(context.resources.getColor(R.color.colorPrimary))
                view.count_like.setTextColor(context.resources.getColor(R.color.colorPrimary))
            } else {
                view.like_image.setColorFilter(context.resources.getColor(R.color.vulcan))
                view.count_like.setTextColor(context.resources.getColor(R.color.vulcan))
            }
        }
    }


    interface FeedAdapterCallback {
        fun likePost(post: PostUI, isLiked: Boolean, position: Int, callback: (PostUI) -> Unit)

        fun openShareView(post: PostUI)

        fun initCategories()
    }
}