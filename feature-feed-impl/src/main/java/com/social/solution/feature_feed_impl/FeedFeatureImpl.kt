package com.social.solution.feature_feed_impl

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedApi
import com.social.solution.feature_feed_api.FeatureFeedStarter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FeedFeatureImpl(
    private val starter: FeatureFeedStarter,
    private val context: Context,
    private val api: FeatureBackendApi
) : FeatureFeedApi {

    override fun feedStarter(): FeatureFeedStarter = starter

    @SuppressLint("CheckResult")
    override fun isAuthorized(): MutableLiveData<Boolean> {
        val result = MutableLiveData<Boolean>()

        api.authApi().isAuthorized()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                if (it == null || !it.isResponseSuccessful || it.data == null) {
                    result.value = false
                } else {
                    result.value = it.data!!.isAllowed
                }
            }, {
                result.value = false
                it.printStackTrace()
            })
        return result
    }
}