package com.social.solution.feature_feed_impl.ui.fragments.feed

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.morozov.core_backend_api.feedMemus.FeedMemusApi
import com.morozov.core_backend_api.feedMemus.modelRequest.FeedGetPostsRequest
import com.morozov.core_backend_api.feedMemus.modelRequest.FeedLikeActionRequest
import com.social.solution.feature_feed_api.models.CategoryUI
import com.social.solution.feature_feed_api.models.PostUI
import com.social.solution.feature_feed_impl.utils.convertFrom

class FeedViewModel : ViewModel() {


    private val categories = MutableLiveData<List<CategoryUI>>()

    private val posts = MutableLiveData<List<PostUI>>()

    private val post = MutableLiveData<PostUI>()


    fun getCategories(): LiveData<List<CategoryUI>> {
        return categories
    }

    fun getPosts(): LiveData<List<PostUI>> {
        return posts
    }

    fun getUpdatePost(): LiveData<PostUI> {
        return post
    }

    @SuppressLint("CheckResult")
    fun loadCategories(feedApi: FeedMemusApi?) {
        feedApi?.getCategories()?.subscribe({ result ->
            Log.i("FEED_VIEW_MODEL", result.toString())
            categories.value = result?.categories?.map { convertFrom(it) }
            Log.e("FEED_VIEW_MODEL", categories.value.toString())
        }, { throwable ->
            Log.e("FEED_VIEW_MODEL", throwable.printStackTrace().toString())
        })
    }

    @SuppressLint("CheckResult")
    fun loadPosts(feedApi: FeedMemusApi?) {
        val bodyGetPost = FeedGetPostsRequest(1, 1, "1", "All memes")
        feedApi?.getPost(bodyGetPost)?.subscribe({ result ->
            posts.value = result?.posts?.map { convertFrom(it) }
            Log.e("FEED_VIEW_MODEL", posts.value.toString())
        }, { throwable ->
            Log.e("FEED_VIEW_MODEL", throwable.printStackTrace().toString())
        })
    }

    @SuppressLint("CheckResult")
    fun like(feedApi: FeedMemusApi?, post: PostUI, isLiked: Boolean) {
        val bodyLikePost = FeedLikeActionRequest(post.id, isLiked)
        feedApi?.likePost(bodyLikePost)?.subscribe({ result ->
            if (posts.value != null && result != null && result.result) {
                this.post.value = convertFrom(result.post!!)
            }
        }, { throwable ->
            Log.e("FEED_VIEW_MODEL", throwable.printStackTrace().toString())
        })
    }

}
